import logo from './logo.svg';
import './App.css';
import TodoItem from './components/TodoItem'
import React, { Component } from 'react';
import TrafficLight from './components/TrafficLight';

const RED = 0;
const ORANGE = 1;
const GREEN = 2;

class App extends Component {

  // constructor() {
  //   super();
  //   this.TodoItem = [
  //     // {title: 'Manh ne', isComplete: true},
  //     // {title: 'Manh hihi'},
  //     // {title: 'buon qua di a'}
  //   ];
  // }

  constructor() {
    super();
    this.state = {
        currentColer: GREEN
    };

    setInterval(() => {
        this.setState({
            currentColer: this.getNextColor(this.state.currentColer)
        });
    }, 1000)
}

getNextColor(color) {
    switch(color) {
        case RED:
            return ORANGE;
        case ORANGE:
            return GREEN;
        default:
            return RED;
    }
}
  render() {
    return (
      <div className="App">
         {/* {this.TodoItem.length > 0 && this.TodoItem.map((title, index) => <TodoItem key={index} title={title}/>)}
         {this.TodoItem.length == 0 && 'No Thing'} */}
        <TrafficLight currentColer={this.state.currentColer}/>
      </div>
    );  
  }
}

export default App;
