import React, { Component } from 'react';
import classNames from 'classname';
import './TrafficLight.css';



const RED = 0;
const ORANGE = 1;
const GREEN = 2;

class TrafficLight extends Component {
    constructor (){
        super()
    }

    render() {
        const { currentColer } = this.props;    
        return <div className="TrafficLight">
            <div className={classNames('bulb', 'red', {active: currentColer === RED})}></div>
            <div className={classNames('bulb', 'orange', {active: currentColer === ORANGE})}></div>
            <div className={classNames('bulb', 'green', {active: currentColer === GREEN})}></div>
        </div>
    }
} 

export default TrafficLight;