import React, { Component } from 'react';
import './TodoItem.css';

class TodoList extends Component {
    render() {
        const { title } = this.props;
        let className = 'TodoItem'
        if (title.isComplete) {
            className += ' TodoItem-complete';
        }
        return (
            <div className={className}>
                    <p>{this.props.title.title}</p>
            </div>
        )
    }
}

export default TodoList;